import { Router } from 'express';
import { $entity$ } from '../database';

const router = Router();

router.route('/$endpoint$s')
//get all $endpoint$
	.get(function(req, res) {
		$entity$.findAll().then(function($endpoint$List) {
			res.send($endpoint$List)
		});
	})

//create a new $endpoint$
	.post(function(req, res) {

		var $endpoint$ = $entity$.create({
			/*
			 * HERE
			 * PUT
			 * THE
			 * ATTRIBUTES
			 * 
			 *
			 */
		}).then(function($endpoint$) {
			res.json($endpoint$);
		}).catch(function(err) {
			res.json({error: err});
		});
	});

router.route('/$endpoint$s/:$endpoint$_id')
//get a single $endpoint$
	.get(function(req, res) {
		$entity$.findById(req.params.$endpoint$_id).then(function(err, $endpoint$) {
			if(err)
				res.json({error: err});
			res.json($endpoint$);
		});
	})

//update a $endpoint$
	.put(function(req, res) {
		$entity$.findById(req.params.$endpoint$_id).then(function(err, $endpoint$) {
			if(err)
				res.json({error: err});

			$endpoint$.update({


				/*
				 * HERE
				 * PUT
				 * THE
				 * ATTRIBUTES
				 * 
				 *
				 */
			}).then(function($endpoint$) {
				res.json($endpoint$);
			}).catch(function(err) {
				res.json({error: err});
			});

		});
	})

//delete a $endpoint$
	.delete(function(req, res) {
		$entity$.remove({
			_id: req.params.$endpoint$_id
		}).then(function(err, $endpoint$) {
			if(err)
				res.json({error: err});
			res.json({result: 'ok'});
		});
	});


export default router;
