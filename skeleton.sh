#!/bin/bash

if [[ $# -lt 2 ]]; then
	echo "Usage:" $0 "<db_dir> <output_dir>"
	exit 1
fi

db_dir=$1
output_dir=$2

echo -e "import express from 'express';\n" > $output_dir/index.js

for entity in `ls $db_dir/models | awk -F'.' '{print $1}'`; do
	if [[ ! $entity == *"_has_"* ]]; then
		epoint=`echo $entity | awk '{ print(tolower($1)) }'`
		cat ./Entity.js | sed -e 's/\$entity\$/'$entity'/g' > $db_dir/entities/$entity.js
		cat ./route.js | sed -e 's/\$entity\$/'$entity'/g' | sed -e 's/\$endpoint\$/'$epoint'/g' > $output_dir/$epoint.js
		echo "import $epoint from './$epoint';" >> $output_dir/index.js
		echo "export const $entity = sequelize.import('entities/$entity');" >> $db_dir/index.js
	fi
done

echo -e "\nconst router = express.Router();\n" >> $output_dir/index.js

for entity in `ls $db_dir/models | awk -F'.' '{print $1}'`; do
	if [[ ! $entity == *"_has_"* ]]; then
		epoint=`echo $entity | awk '{ print(tolower($1)) }'`
		echo "router.use($epoint);" >> $output_dir/index.js
	fi
done

echo -e "\nexport { sequelize };" >> $db_dir/index.js

echo -e "\nexport default router;" >> $output_dir/index.js

exit 0
