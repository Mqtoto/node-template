import create$entity$ from '../models/$entity$';

export default function(sequelize, DataTypes) {
	const $entity$ = create$entity$(sequelize, DataTypes);

	$entity$.findById = function(id$entity$) {
		return $entity$.findOne({
			where: {
				id$entity$
			}});
	};

	return $entity$;
}
